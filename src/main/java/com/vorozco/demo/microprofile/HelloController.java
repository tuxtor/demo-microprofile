package com.vorozco.demo.microprofile;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 */
@Path("/hello")
@Singleton
public class HelloController {

    @Inject
    Edwin edwin;

    @GET
    public String sayHello() {
        return edwin.saludar();
    }

    @GET
    @Path("/pajas")
    public String sayPajas() {
        return edwin.hablarPajas() + " 0.0.1";
    }
}
