package com.vorozco.demo.microprofile;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EdwinTest {

    @Test
    public void testSaludar(){
        Edwin ed = new Edwin();
        assertEquals("Hola tux", ed.saludar());
    }

      @Test
    public void testDespedir(){
        Edwin ed = new Edwin();
        assertEquals("Adios desde Nimajuyu", ed.despedir());
    }

    @Test
    public void testHablarPajas(){
        Edwin ed = new Edwin();
        assertEquals("Antes fui pollero", ed.hablarPajas());
    }

}